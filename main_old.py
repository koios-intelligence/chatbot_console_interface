#! /usr/bin/python3

"""Store input in a queue as soon as it arrives, and work on
it as soon as possible. Do something with untreated input if
the user interrupts. Do other stuff if idle waiting for
input."""

import sys
import time
import threading
import queue
import random
import string
from pymongo import MongoClient

mlab = 'mongodb://heroku_42dbxrj1:2cb30f0dsvne3g26aqg8fgjlos@ds257077.mlab.com:57077/heroku_42dbxrj1'
mlab_db = 'heroku_42dbxrj1'


class Bot(object):

    def __init__(self, api):
        self.api = api

    def send_text_message(self, id, message):
        """
        Send a text message
        """
        if self.api == 'terminal':
            print(message)
            return message
        else:
            raise NotImplementedError

    def send_quick_reply(self, id, message, quick_replies):
        if self.api == 'terminal':
            message += '\n'
            for i, reply in enumerate(quick_replies):
                message += "%i - %s \n" % (i, reply['title'])
            print(message)
            return message
        else:
            raise NotImplementedError

    def loop(self, sender_id, message):
        """
        Loop through the GRAPH and
        update the data base

        :param sender_id:
            id of the sender
        :param recipient_id:
            id of the convo
        :param message:
            String
        :return:
            POST request with the question/answer
        """
        print('-' * 80)
        print('message: %s' % message)
        print('-' * 80)
        mongo_client = MongoClient()
        users = mongo_client[mlab_db]['users']
        user_credentials = {'sender_id': sender_id}
        print(sender_id)
        user = users.find_one(user_credentials)

        # If the user doesn't exist in the DB
        # create a new entry
        if user is None:
            users.insert_one(user_credentials)
            state = 'faq'
            users.update_one(user_credentials,
                             {
                                 '$set': {
                                     'state': state,
                                 }
                             })
        else:
            state = user['state']
            print(state)

        if state == 'faq':
            # Check if the user wants an answer to some question
            # or if we need to instantiate a session

            # TODO: CHANGE THIS
            message_tokens = message.split(' ')
            if 'home' in message_tokens:
                # Initiate "buy a home insurance" session
                next_state = 'home_start'

            else:
                next_state = 'faq'

            if next_state != 'faq':
                # Update the state
                users.update_one(user_credentials,
                                 {
                                     '$set': {
                                         'state': next_state,
                                     }
                                 })
                if 'quick_reply' in GRAPH[next_state]:
                    self.send_quick_reply(id, GRAPH[next_state]['message'],
                                          GRAPH[next_state]['quick_reply'])
                else:
                    self.send_text_message(id,
                                       GRAPH[next_state]['message'])
            else:
                # Update the state
                users.update_one(user_credentials,
                                 {
                                     '$set': {
                                         'state': 'faq',
                                     }
                                 })
                self.send_text_message(id, 'this is where I answer a FAQ')
        else:

            # Check if the user sent a good answer
            callable_rule = GRAPH[state]['next']
            bool, next_state, answer = callable_rule(message)

            if next_state is not None:
                users.update_one(user_credentials,
                                 {
                                     '$set': {
                                         'state': next_state,
                                     }
                                 })
                if 'quick_reply' in GRAPH[next_state]:
                    self.send_quick_reply(id, GRAPH[next_state]['message'],
                                          GRAPH[next_state]['quick_reply'])
                else:
                    self.send_text_message(id,
                                       GRAPH[next_state]['message'])
            else:
                # if next_state is none then the user entered
                # an invalid answer; eventually, we can add the option
                # to check FAQ, but right now, we only return the
                # "lost" message
                # TODO: Add the option to quit the session (return to FAQ state)
                if message == 'quit':
                    # return to faq state
                    users.update_one(user_credentials,
                                     {
                                         '$set': {
                                             'state': 'faq',
                                         }
                                     })
                    self.send_text_message(id, "Successfully ended the session")
                else:
                    self.send_text_message(id,
                                           GRAPH[state]['lost'])


id = ''.join(random.SystemRandom().choice(
             string.ascii_uppercase + string.digits) for _ in range(15))
timeout = 0.1  # seconds
last_work_time = time.time()


def treat_input(linein):
    global last_work_time
    # print('Working on line:', linein, end='')
    print(linein)
    time.sleep(1)  # working takes time

    # This is where you call the bot method!!
    #
    bot = Bot(api='terminal')
    bot.loop(id, linein['message'])

    # print('Done working on line:', linein, end='')
    last_work_time = time.time()


def idle_work():
    global last_work_time
    now = time.time()

    # do some other stuff every 2 seconds of idleness
    if now - last_work_time > 2:
        # print('Idle for too long; doing some other stuff.')
        last_work_time = now


def input_cleanup():

    while not input_queue.empty():
        line = input_queue.get()
        print("Didn't get to work on this line:", line, end='')


# will hold all input read, until the work thread has chance
# to deal with it
input_queue = queue.Queue()

# will signal to the work thread that it should exit when
# it finishes working on the currently available input
no_more_input = threading.Lock()
no_more_input.acquire()

# will signal to the work thread that it should exit even if
# there's still input available
interrupted = threading.Lock()
interrupted.acquire()


# work thread' loop: work on available input until main
# thread exits
def treat_input_loop():
    while not interrupted.acquire(blocking=False):
        try:
            treat_input(input_queue.get(timeout=timeout))
        except queue.Empty:
            # if no more input, exit
            if no_more_input.acquire(blocking=False):
                break
            else:
                idle_work()
    print('Work loop is done.')


work_thread = threading.Thread(target=treat_input_loop)
work_thread.start()

# main loop: stuff input in the queue until there's either
# no more input, or the program gets interrupted
try:
    for line in sys.stdin:
        if line:  # optional: skipping empty lines
            input_queue.put({'message': line[:-1],
                             'id': id})

    # inform work loop that there will be no new input and it
    # can exit when done
    no_more_input.release()

    # wait for work thread to finish
    work_thread.join()

except KeyboardInterrupt:
    interrupted.release()
    input_cleanup()

print('Main loop is done.')