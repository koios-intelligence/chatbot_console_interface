import requests
import random
import json


def main():
    data = {}
    random.seed(a=None)
    data['user_id'] = str(random.random()*1000000)
    data['lang'] = "en"
    while True:
        datajson = json.dumps(data)
        response = requests.post('http://127.0.0.1:5001', data=datajson)
        res = json.loads(response.content)
        # print(">>>", res)
        print("Koios:", res["message"])
        payload = res["payload"]
        for i, choice in enumerate(payload):
            print(str(i+1)+".", choice["message_sent"])
        msg = input("> ").strip()
        if (msg == "fr" or msg == "en") and msg != data['lang']:
            data['lang'] = msg
        else:
            if res["reply_type"] == "checkbox":
                try:
                    nums = msg.replace(",", " ").split(" ")
                    answer = []
                    for num in nums:
                        if num.strip().isdigit():
                            val = int(num)-1
                            if 0 <= val < len(payload):
                                answer.append(payload[val]["payload"])
                    data['payload'] = answer
                except (KeyError, IndexError):
                    data['payload'] = msg
            else:
                try:
                    num = int(msg)
                    data['payload'] = payload[num-1]["payload"]
                except (KeyError, IndexError):
                    data['payload'] = msg
            data['reply_type'] = res['reply_type']
            data['state'] = res['state']


main()
